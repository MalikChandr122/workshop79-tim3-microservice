package com.example.inventoryservice.service.impl;

import com.example.inventoryservice.db.repository.InventoryRepository;
import com.example.inventoryservice.dto.Inventory;
import com.example.inventoryservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {
    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public List<Inventory> findAll() {
        return compose(inventoryRepository.findAll());
    }

    @Override
    public void save(Inventory inventory) {
        inventoryRepository.save(com.example.inventoryservice.db.Inventory.builder()
                        .skuCode(inventory.getSkuCode())
                        .stock(inventory.getStock())
                        .build());
    }

    @Override
    public void delete(Integer id) {
        inventoryRepository.delete(com.example.inventoryservice.db.Inventory.builder().id(String.valueOf(id)).build());
    }

    private List<Inventory> compose(List<com.example.inventoryservice.db.Inventory> inventories) {
        List<Inventory> results = new ArrayList<>();
        for (int i = 0; i < inventories.size(); i++) {
            var inventory = Inventory.builder()
                    .id(inventories.get(i).getId())
                    .skuCode(inventories.get(i).getSkuCode())
                    .stock(inventories.get(i).getStock())
                    .build();
            results.add(inventory);
        }
        return results;
    }
}
