package com.example.inventoryservice.service;

import com.example.inventoryservice.dto.Inventory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InventoryService {
    List<Inventory> findAll();
    void save(Inventory inventory);
    void delete(Integer id);
}
