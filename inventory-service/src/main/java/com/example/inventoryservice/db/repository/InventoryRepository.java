package com.example.inventoryservice.db.repository;

import com.example.inventoryservice.db.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("InventoryRepository")
public interface InventoryRepository extends JpaRepository<Inventory, String> {
}
