package com.example.discoveryserverservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AppDiscover {
	public static void main(String[] args) {
		SpringApplication.run(AppDiscover.class, args);
	}
}
