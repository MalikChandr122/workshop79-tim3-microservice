package com.example.orderservice.service;

import com.example.basedomains.dto.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {
    List<Order> findAll();
    void save(Order order);
    void delete(Integer id);
}
