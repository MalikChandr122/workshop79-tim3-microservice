package com.example.orderservice.service.impl;

import com.example.basedomains.dto.Order;
import com.example.basedomains.dto.OrderEvent;
import com.example.orderservice.db.repository.OrderRepository;
import com.example.orderservice.producer.OrderProducer;
import com.example.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderProducer orderProducer;

    @Override
    public List<Order> findAll() {
        return compose(orderRepository.findAll());
    }

    @Override
    public void save(Order order) {
        orderRepository.save(com.example.orderservice.db.Order.builder()
                        .productId(Long.valueOf(order.getProductId()))
                        .quantity(Long.valueOf(order.getQuantity()))
                        .price(order.getPrice())
                        .build());
        System.out.println("Successfully save to DB....");

        OrderEvent orderEvent = new OrderEvent();
        orderEvent.setStatus("PENDING");
        orderEvent.setMessage("order status is in pending state");
        orderEvent.setOrder(order);

        orderProducer.sendMessage(orderEvent);
        System.out.println("Successfully send to queue....");
    }

    @Override
    public void delete(Integer id) {
        orderRepository.deleteById(String.valueOf(id));
    }

    private List<Order> compose(List<com.example.orderservice.db.Order> orders) {
        List<Order> results = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            var inventory = Order.builder()
                    .id(orders.get(i).getId())
                    .productId(String.valueOf(orders.get(i).getProductId()))
                    .quantity(orders.get(i).getQuantity().intValue())
                    .price(orders.get(i).getPrice())
                    .build();
            results.add(inventory);
        }
        return results;
    }
}
