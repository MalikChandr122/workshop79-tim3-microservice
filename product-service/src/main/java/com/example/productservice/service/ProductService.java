package com.example.productservice.service;

import com.example.productservice.dto.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> findAll();
    void save(Product product);
    void delete(Integer id);
}
