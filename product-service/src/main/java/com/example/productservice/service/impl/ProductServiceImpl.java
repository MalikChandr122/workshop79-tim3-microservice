package com.example.productservice.service.impl;

import com.example.productservice.dto.Product;
import com.example.productservice.db.repository.ProductRepository;
import com.example.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return compose(productRepository.findAll());
    }

    @Override
    public void save(Product product) {
        productRepository.save(com.example.productservice.db.Product.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build());
    }

    @Override
    public void delete(Integer id) {
        productRepository.delete(com.example.productservice.db.Product.builder().id(String.valueOf(id)).build());
    }

    private List<Product> compose(List<com.example.productservice.db.Product> products) {
        List<Product> results = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            var product = Product.builder()
                    .id(products.get(i).getId())
                    .name(products.get(i).getName())
                    .description(products.get(i).getDescription())
                    .price(products.get(i).getPrice())
                    .build();
            results.add(product);
        }
        return results;
    }
}
